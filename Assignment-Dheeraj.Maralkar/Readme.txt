The package contains spring boot application for weather forecasting assignment.

Main class: StartWebApplication

Run this class to start application

Url End Point: http://localhost:8080/v1/forecast  (You can use normal browser to view this)
This end point shows index page for the application

Other end point:
GET /v1/forecast/cities/:zipcode
get response for weather forecasting (JSON response)

application.properties file contains 
external microserver from which the actual data get extracted and its api key
Please do not change those values