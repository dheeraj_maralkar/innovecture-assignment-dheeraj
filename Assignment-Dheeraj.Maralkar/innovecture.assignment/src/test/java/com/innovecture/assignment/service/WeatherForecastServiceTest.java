package com.innovecture.assignment.service;

import com.innovecture.assignment.dto.ForecastResponse;
import com.innovecture.assignment.dto.openWeather.City;
import com.innovecture.assignment.dto.openWeather.OpenWeatherResponse;
import com.innovecture.assignment.dto.openWeather.Temperature;
import com.innovecture.assignment.dto.openWeather.WeatherDetails;
import com.innovecture.assignment.helper.WebServiceHelper;
import org.hamcrest.CoreMatchers;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.*;

import static org.junit.Assert.*;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class WeatherForecastServiceTest {

    private static String zipCode_valid = "123";
    private static String zipCode_invalid = "456";
    private static Float minTemperature = Float.MAX_VALUE;

//    @TestConfiguration
    static class WeatherForecastServiceTestConfig{

        @Bean
        public WebServiceHelper webServiceHelper(){
            WebServiceHelper webServiceHelperMock = Mockito.mock(WebServiceHelper.class);
            OpenWeatherResponse openWeatherResponse = new OpenWeatherResponse();
            openWeatherResponse.setCode("404");
            openWeatherResponse.setMessage("Incorrect zipcode");
            when(webServiceHelperMock.buildWsReqAndResp(Mockito.contains("?q="+zipCode_invalid),Mockito.any(),Mockito.any(),Mockito.any(),Mockito.any()))
                    .thenReturn(openWeatherResponse);

            when(webServiceHelperMock.buildWsReqAndResp(Mockito.contains("?q="+zipCode_valid),Mockito.any(),Mockito.any(),Mockito.any(),Mockito.any()))
                    .thenReturn(getOpenWeatherHappyResponse());

            return webServiceHelperMock;
        }

        private OpenWeatherResponse getOpenWeatherHappyResponse() {
            Random r = new Random();
            TimeZone.setDefault(TimeZone.getTimeZone("EST"));
            OpenWeatherResponse openWeatherResponse = new OpenWeatherResponse();
            City city = new City(); city.setCountry("Test Country"); city.setName("Test Name");
            List<WeatherDetails> weatherDetails = new ArrayList<>();
            openWeatherResponse.setCode("200");
            openWeatherResponse.setMessage("DONE");
            openWeatherResponse.setCity(city);
            openWeatherResponse.setWeatherDetails(weatherDetails);
            Date today = new Date();
            System.out.println("Today:"+today);
            Calendar cal = Calendar.getInstance();
            cal.setTime(today);
            for (int i=0;i<24*3;i++){
                cal.add(Calendar.MINUTE,60);
                Float temp = r.nextFloat();
                if(minTemperature>temp && today.getDate()==cal.get(Calendar.DATE)-1) minTemperature=temp;
                Temperature temperature = new Temperature();
                temperature.setTempMin(temp);
                WeatherDetails wd = new WeatherDetails();
                wd.setTemperature(temperature);
                wd.setTime(cal.getTime().getTime()/1000);
                weatherDetails.add(wd);
            }

            return openWeatherResponse;
        }

        @Bean
        public WeatherForecastService weatherForecastService(){ return new WeatherForecastService(); }
    }

    @Autowired
    WeatherForecastService weatherForecastService;

    @Test
    public void testForecastForInvalidZipCode() {
        ForecastResponse forecastResponse = weatherForecastService.getForcast(zipCode_invalid);
        assertEquals("404", forecastResponse.getCode());
        assertEquals("Incorrect zipcode", forecastResponse.getMessage());
    }

    @Test
    public void testForecastForMinTemperature() {
        ForecastResponse forecastResponse = weatherForecastService.getForcast(zipCode_valid);
        assertEquals("200", forecastResponse.getCode());
        assertThat(forecastResponse.getMessage(), CoreMatchers.containsString("with temperature:"+minTemperature));
    }


}