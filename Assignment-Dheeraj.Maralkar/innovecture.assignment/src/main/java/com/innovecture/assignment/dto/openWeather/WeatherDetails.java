
package com.innovecture.assignment.dto.openWeather;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class WeatherDetails {

    @JsonProperty("dt")
    private Long time;

    @JsonProperty("main")
    private Temperature temperature;

    @JsonIgnore
    public Date getDateTime(){
        return new Date(time*1000);
    }

    public Temperature getTemperature() {
        return temperature;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public void setTemperature(Temperature temperature) {
        this.temperature = temperature;
    }
}

