package com.innovecture.assignment.logger;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.context.annotation.Configuration;

@Aspect
@Configuration
public class MethodTimeLogger {

    /**
     * The logger.
     */
    private Logger logger = LogManager.getLogger();

    /**
     * Log in and out time for each annotated method with annotation LogInOut
     *
     * @param joinPoint
     * @return object return from original method
     * @throws Throwable
     */
    @Around("@annotation(com.innovecture.assignment.logger.LogInOut)")
    public Object serviceProcessTime(ProceedingJoinPoint joinPoint) throws Throwable {
        long startTime = System.currentTimeMillis();
        String source = joinPoint.getTarget().getClass().getSimpleName() + ":" + joinPoint.getSignature().getName();
        logger.info(source+" In ");
        Object ret = joinPoint.proceed();
        logger.info(source +" Out\t: responseTime " + (System.currentTimeMillis()-startTime) + "Millis");
        return ret;
    }
}
