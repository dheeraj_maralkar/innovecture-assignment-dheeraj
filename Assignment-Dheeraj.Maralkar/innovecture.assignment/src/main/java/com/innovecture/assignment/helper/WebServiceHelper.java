package com.innovecture.assignment.helper;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.innovecture.assignment.dto.openWeather.OpenWeatherResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.Arrays;


/**
 * The class CSSWebServiceHelper
 *
 * @author Nilam Dhami
 */
@Component
public class WebServiceHelper {

    /**
     * The logger.
     */
    private Logger logger = LogManager.getLogger();

    private ObjectMapper objectMapper = new ObjectMapper();

    private static HttpHeaders getHttpHeadersWithClientCredentials() {
        HttpHeaders headers = getHttpHeaders();
        return headers;
    }

    /**
     * get http headers.
     */
    private static HttpHeaders getHttpHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        return headers;
    }

    /**
     * build web service request and response.
     *
     * @param endpointURL
     * @param queryParams
     * @param returnObject
     * @param httpMethodName
     * @param postObject
     * @return Response object from API call
     */

    public Object buildWsReqAndResp(String endpointURL,
                                    MultiValueMap<String, String> queryParams,
                                    Class<?> returnObject,
                                    HttpMethod httpMethodName,
                                    Object postObject) {

        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        restTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory());
        URI uri;
        HttpEntity<Object> entity;
        HttpHeaders headers = getHttpHeadersWithClientCredentials();
        try {
            if (httpMethodName.equals(HttpMethod.POST) || httpMethodName.equals(HttpMethod.PUT) || httpMethodName.equals(HttpMethod.DELETE)) {
                uri = UriComponentsBuilder.fromHttpUrl(endpointURL).queryParams(queryParams).build().encode().toUri();
                entity = new HttpEntity<>(postObject, headers);
            } else {
                uri = UriComponentsBuilder.fromHttpUrl(endpointURL).queryParams(queryParams).build().encode().toUri();
                entity = new HttpEntity<>(headers);
            }
            ResponseEntity<?> response = restTemplate.exchange(uri, httpMethodName, entity, returnObject);
            if (response != null) {
                return response.getBody();
            }
        }catch (HttpClientErrorException e) {
            if (e.getRawStatusCode() == 404) {
                OpenWeatherResponse openWeatherResponse = new OpenWeatherResponse();
                openWeatherResponse.setCode("404");
                openWeatherResponse.setMessage("Incorrect zipcode");
                return openWeatherResponse;
            }
        }
        catch (Exception e) {
            e.getMessage();
            OpenWeatherResponse openWeatherResponse = new OpenWeatherResponse();
            openWeatherResponse.setCode("503");
            openWeatherResponse.setMessage("Error while getting details");
            return openWeatherResponse;
        }
        return null;
    }
}