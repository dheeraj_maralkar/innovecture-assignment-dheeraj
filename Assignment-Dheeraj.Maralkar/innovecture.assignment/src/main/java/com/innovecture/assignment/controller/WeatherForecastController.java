package com.innovecture.assignment.controller;

import com.innovecture.assignment.dto.ForecastResponse;
import com.innovecture.assignment.service.WeatherForecastService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("v1/forecast/cities/{zipcode}")
public class WeatherForecastController {

    @Autowired
    WeatherForecastService weatherForecastService;

    /**
     * Gets the forecast response for given zipcode
     * The welcome page will have the text field for zip code
     *
     * @param zipcode zipcode of city of US
     * @return forecast response with coolest hour on next day
     */
    @GetMapping
    public ForecastResponse getForecast(@PathVariable String zipcode){
        return weatherForecastService.getForcast(zipcode);
    }
}
