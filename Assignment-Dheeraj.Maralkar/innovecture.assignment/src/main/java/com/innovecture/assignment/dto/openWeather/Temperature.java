
package com.innovecture.assignment.dto.openWeather;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Temperature {

    @JsonProperty("temp")
    private Float temp;

    @JsonProperty("temp_min")
    private Float tempMin;

    @JsonProperty("temp_max")
    private Float tempMax;

    public Float getTemp() {
        return temp;
    }

    public Float getTempMin() {
        return tempMin;
    }

    public Float getTempMax() {
        return tempMax;
    }

    public void setTemp(Float temp) {
        this.temp = temp;
    }

    public void setTempMin(Float tempMin) {
        this.tempMin = tempMin;
    }

    public void setTempMax(Float tempMax) {
        this.tempMax = tempMax;
    }
}
