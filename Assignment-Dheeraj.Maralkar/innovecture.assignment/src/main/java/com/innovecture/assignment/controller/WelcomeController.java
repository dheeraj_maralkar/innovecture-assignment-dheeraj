package com.innovecture.assignment.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("v1/forecast")
public class WelcomeController {

    /**
     * Gets the view for the index
     * The welcome page will have the text field for zip code
     *
     * @return the view name for welcome page/index page
     */
    @GetMapping
    public String main() {
        return "welcome";
    }


}