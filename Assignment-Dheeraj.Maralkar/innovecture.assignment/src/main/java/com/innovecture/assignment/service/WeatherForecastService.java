package com.innovecture.assignment.service;

import com.innovecture.assignment.Contants.ServiceResponse;
import com.innovecture.assignment.dto.ForecastResponse;
import com.innovecture.assignment.dto.openWeather.OpenWeatherResponse;
import com.innovecture.assignment.dto.openWeather.WeatherDetails;
import com.innovecture.assignment.helper.WebServiceHelper;
import com.innovecture.assignment.logger.LogInOut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Date;
import java.util.Optional;

@Service
public class WeatherForecastService {

    @Autowired
    WebServiceHelper webServiceHelper;

    @Value("${openWeatherMapUrl}")
    private String openWeatherMapUrl;

    @Value("${forcastServiceApiKey}")
    private String openWeatherMapApiKey;

    /**
     * get forecasted temperature for tomorrow and coolest hour
     *
     * @param zipCode of the city in US
     * @return Forecast response with message
     */
    @LogInOut
    public ForecastResponse getForcast(String zipCode) {
        String apiUrl = openWeatherMapUrl + "?q=" + zipCode + ",us&APPID=" + openWeatherMapApiKey;
        OpenWeatherResponse openWeatherResponse = (OpenWeatherResponse)
                webServiceHelper.buildWsReqAndResp
                        (apiUrl, null, OpenWeatherResponse.class, HttpMethod.GET, null);

        if (!openWeatherResponse.getCode().equals("200"))
            return ServiceResponse.getErrorForecastResponse(openWeatherResponse);

        WeatherDetails lowest = findLowestTempHourForTomorrow(openWeatherResponse);
        return ServiceResponse.getSuccessForecastResponse(openWeatherResponse, lowest);
    }

    private WeatherDetails findLowestTempHourForTomorrow(OpenWeatherResponse openWeatherResponse) {
        Date todayEnd = getTodayEndTime();
        Date tomorrowEnd = getTomorrowEndTime();
        Optional<WeatherDetails> min = openWeatherResponse.getWeatherDetails().stream().filter(
                wd -> wd.getDateTime().after(todayEnd) && wd.getDateTime().before(tomorrowEnd)
        ).min((w1, w2) -> Float.compare(w1.getTemperature().getTempMin(), w2.getTemperature().getTempMin()));

        if (min.isPresent()) {
            return min.get();
        }
        return null;

    }

    private Date getTodayEndTime() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.set(Calendar.HOUR_OF_DAY, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        return cal.getTime();
    }

    private Date getTomorrowEndTime() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(getTodayEndTime());
        cal.add(Calendar.DATE, 1);
        return cal.getTime();
    }

}
