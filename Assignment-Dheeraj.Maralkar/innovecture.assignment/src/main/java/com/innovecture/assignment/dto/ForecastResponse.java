package com.innovecture.assignment.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({"error_status", "code", "message", "data"})
public class ForecastResponse {
    /**
     * The error status.
     */
    @JsonProperty("error_status")
    private Boolean errorStatus = true;

    /**
     * The code.
     */
    @JsonProperty("code")
    private String code;

    /**
     * The message.
     */
    @JsonProperty("message")
    private String message;

    public Boolean getErrorStatus() {
        return errorStatus;
    }

    public void setErrorStatus(Boolean errorStatus) {
        this.errorStatus = errorStatus;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
