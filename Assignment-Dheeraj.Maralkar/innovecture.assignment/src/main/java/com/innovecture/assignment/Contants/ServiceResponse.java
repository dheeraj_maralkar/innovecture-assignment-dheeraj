package com.innovecture.assignment.Contants;

import com.innovecture.assignment.dto.ForecastResponse;
import com.innovecture.assignment.dto.openWeather.OpenWeatherResponse;
import com.innovecture.assignment.dto.openWeather.WeatherDetails;

public final class ServiceResponse {

    public static ForecastResponse getSuccessForecastResponse(OpenWeatherResponse openWeatherResponse, WeatherDetails lowest) {
        ForecastResponse forecastResponse = new ForecastResponse();
        forecastResponse.setCode("200");
        forecastResponse.setErrorStatus(false);
        forecastResponse.setMessage("For the city : "
                + openWeatherResponse.getCity().getName()
                + ", the coolest hour of the day will be: "
                + lowest.getDateTime()
                + " (with temperature:" + lowest.getTemperature().getTempMin() + ")");
        return forecastResponse;
    }


    public static ForecastResponse getErrorForecastResponse(OpenWeatherResponse openWeatherResponse) {
        ForecastResponse forecastResponse = new ForecastResponse();
        forecastResponse.setCode(openWeatherResponse.getCode());
        forecastResponse.setErrorStatus(true);
        forecastResponse.setMessage(openWeatherResponse.getMessage());
        return forecastResponse;
    }


}
